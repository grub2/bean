# -*- makefile -*-

# For grub-mkelfimage.
bin_UTILITIES += grub-mkelfimage
grub_mkelfimage_SOURCES = util/elf/grub-mkimage.c util/misc.c \
	util/resolve.c
util/elf/grub-mkimage.c_DEPENDENCIES = Makefile

# For grub-probe.
sbin_UTILITIES += grub-probe
util/grub-probe.c_DEPENDENCIES = grub_probe_init.h
grub_probe_SOURCES = util/grub-probe.c	\
	util/hostdisk.c	util/misc.c util/getroot.c		\
	kern/device.c kern/disk.c kern/err.c kern/misc.c	\
	kern/parser.c kern/partition.c kern/file.c		\
	\
	fs/affs.c fs/cpio.c fs/fat.c fs/ext2.c fs/hfs.c		\
	fs/hfsplus.c fs/iso9660.c fs/udf.c fs/jfs.c fs/minix.c	\
	fs/ntfs.c fs/ntfscomp.c fs/reiserfs.c fs/sfs.c		\
	fs/ufs.c fs/xfs.c fs/afs.c fs/tar.c			\
	\
	partmap/pc.c partmap/apple.c partmap/sun.c partmap/gpt.c\
	kern/fs.c kern/env.c fs/fshelp.c			\
	disk/raid.c disk/mdraid_linux.c disk/lvm.c grub_probe_init.c

ifeq ($(enable_grub_fstest), yes)
bin_UTILITIES += grub-fstest
endif

# For grub-fstest.
util/grub-fstest.c_DEPENDENCIES = grub_fstest_init.h
grub_fstest_SOURCES = util/grub-fstest.c util/hostfs.c util/misc.c 	\
	kern/file.c kern/device.c kern/disk.c kern/err.c kern/misc.c	\
	disk/host.c disk/loopback.c kern/list.c kern/command.c		\
	lib/arg.c commands/extcmd.c normal/datetime.c normal/misc.c	\
	lib/hexdump.c lib/crc.c commands/blocklist.c commands/ls.c 	\
	\
	fs/affs.c fs/cpio.c fs/fat.c fs/ext2.c fs/hfs.c			\
	fs/hfsplus.c fs/iso9660.c fs/udf.c fs/jfs.c fs/minix.c		\
	fs/ntfs.c fs/ntfscomp.c fs/reiserfs.c fs/sfs.c			\
	fs/ufs.c fs/xfs.c fs/afs.c fs/tar.c				\
	\
	kern/partition.c partmap/pc.c partmap/apple.c partmap/sun.c	\
	partmap/gpt.c							\
	kern/fs.c kern/env.c fs/fshelp.c disk/raid.c			\
	disk/raid5_recover.c disk/raid6_recover.c 			\
	disk/mdraid_linux.c disk/dmraid_nvidia.c disk/lvm.c 		\
	grub_fstest_init.c

# For grub-mkfont.
ifeq ($(enable_grub_mkfont), yes)
bin_UTILITIES += grub-mkfont
grub_mkfont_SOURCES = util/grub-mkfont.c util/misc.c
grub_mkfont_CFLAGS = $(freetype_cflags)
grub_mkfont_LDFLAGS = $(freetype_libs)
endif

# For the parser.
grub_script.tab.c grub_script.tab.h: script/sh/parser.y
	$(YACC) -d -p grub_script_yy -b grub_script $(srcdir)/script/sh/parser.y
DISTCLEANFILES += grub_script.tab.c grub_script.tab.h

# For grub-emu.
grub_emu_init.lst: geninit.sh $(filter-out grub_emu_init.c,$(grub_emu_SOURCES))
	rm -f $@; grep GRUB_MOD_INIT $(filter %.c,$^) /dev/null > $@
DISTCLEANFILES += grub_emu_init.lst

grub_emu_init.h: grub_emu_init.lst $(filter-out grub_emu_init.c,$(grub_emu_SOURCES)) geninitheader.sh
	rm -f $@; sh $(srcdir)/geninitheader.sh $< > $@
DISTCLEANFILES += grub_emu_init.h

grub_emu_init.c: grub_emu_init.lst $(filter-out grub_emu_init.c,$(grub_emu_SOURCES)) geninit.sh grub_emu_init.h
	rm -f $@; sh $(srcdir)/geninit.sh $< $(filter %.c,$^) > $@
DISTCLEANFILES += grub_emu_init.c

# For grub-probe.
grub_probe_init.lst: geninit.sh $(filter-out grub_probe_init.c,$(grub_probe_SOURCES))
	rm -f $@; grep GRUB_MOD_INIT $(filter %.c,$^) /dev/null > $@
DISTCLEANFILES += grub_probe_init.lst

grub_probe_init.h: grub_probe_init.lst $(filter-out grub_probe_init.c,$(grub_probe_SOURCES)) geninitheader.sh
	rm -f $@; sh $(srcdir)/geninitheader.sh $< > $@
DISTCLEANFILES += grub_probe_init.h

grub_probe_init.c: grub_probe_init.lst $(filter-out grub_probe_init.c,$(grub_probe_SOURCES)) geninit.sh grub_probe_init.h
	rm -f $@; sh $(srcdir)/geninit.sh $< $(filter %.c,$^) > $@
DISTCLEANFILES += grub_probe_init.c

# For grub-setup.
grub_setup_init.lst: geninit.sh $(filter-out grub_setup_init.c,$(grub_setup_SOURCES))
	rm -f $@; grep GRUB_MOD_INIT $(filter %.c,$^) /dev/null > $@
DISTCLEANFILES += grub_setup_init.lst

grub_setup_init.h: grub_setup_init.lst $(filter-out grub_setup_init.c,$(grub_setup_SOURCES)) geninitheader.sh
	rm -f $@; sh $(srcdir)/geninitheader.sh $< > $@
DISTCLEANFILES += grub_setup_init.h

grub_setup_init.c: grub_setup_init.lst $(filter-out grub_setup_init.c,$(grub_setup_SOURCES)) geninit.sh grub_setup_init.h
	rm -f $@; sh $(srcdir)/geninit.sh $< $(filter %.c,$^) > $@
DISTCLEANFILES += grub_setup_init.c

# For grub-fstest.
grub_fstest_init.lst: geninit.sh $(filter-out grub_fstest_init.c,$(grub_fstest_SOURCES))
	rm -f $@; grep GRUB_MOD_INIT $(filter %.c,$^) /dev/null > $@
DISTCLEANFILES += grub_fstest_init.lst

grub_fstest_init.h: grub_fstest_init.lst $(filter-out grub_fstest_init.c,$(grub_fstest_SOURCES)) geninitheader.sh
	rm -f $@; sh $(srcdir)/geninitheader.sh $< > $@
DISTCLEANFILES += grub_fstest_init.h

grub_fstest_init.c: grub_fstest_init.lst $(filter-out grub_fstest_init.c,$(grub_fstest_SOURCES)) geninit.sh grub_fstest_init.h
	rm -f $@; sh $(srcdir)/geninit.sh $< $(filter %.c,$^) > $@
DISTCLEANFILES += grub_fstest_init.c

# for grub-editenv
bin_UTILITIES += grub-editenv
grub_editenv_SOURCES = util/grub-editenv.c lib/envblk.c util/misc.c kern/misc.c kern/err.c
CLEANFILES += grub-editenv

# for grub-pe2elf
ifeq ($(enable_grub_pe2elf), yes)
bin_UTILITIES += grub-pe2elf
endif

grub_pe2elf_SOURCES = util/grub-pe2elf.c util/misc.c
CLEANFILES += grub-pe2elf

# grub_macho2img assumes a lot about source file.
# So installing it definitively is useless
# But adding to bin_UTILITIES is needed for
# genmk.rb to work
ifeq (0,1)
bin_UTILITIES += grub-macho2img
endif
grub_macho2img_SOURCES = util/grub-macho2img.c
CLEANFILES += grub-macho2img

# For grub-mkconfig
grub-mkconfig: util/grub-mkconfig.in config.status
	./config.status --file=$@:$<
	chmod +x $@
sbin_SCRIPTS += grub-mkconfig
CLEANFILES += grub-mkconfig

grub-mkconfig_lib: util/grub-mkconfig_lib.in config.status
	./config.status --file=$@:$<
	chmod +x $@
lib_SCRIPTS += grub-mkconfig_lib
CLEANFILES += grub-mkconfig_lib

update-grub_lib: util/update-grub_lib.in config.status
	./config.status --file=$@:$<
	chmod +x $@
lib_SCRIPTS += update-grub_lib
CLEANFILES += update-grub_lib

%: util/grub.d/%.in config.status
	./config.status --file=$@:$<
	chmod +x $@
grub-mkconfig_SCRIPTS = 00_header 30_os-prober 40_custom
ifneq (, $(host_kernel))
grub-mkconfig_SCRIPTS += 10_$(host_kernel)
endif

CLEANFILES += $(grub-mkconfig_SCRIPTS)

grub-mkconfig_DATA += util/grub.d/README

# For grub-dumpbios
grub-dumpbios: util/grub-dumpbios.in config.status
	./config.status --file=$@:$<
	chmod +x $@
sbin_SCRIPTS += grub-dumpbios
CLEANFILES += grub-dumpbios

# Filing systems.
pkglib_MODULES += fshelp.mod fat.mod ufs.mod ext2.mod ntfs.mod		\
	ntfscomp.mod minix.mod hfs.mod jfs.mod iso9660.mod xfs.mod	\
	affs.mod sfs.mod hfsplus.mod reiserfs.mod cpio.mod tar.mod	\
	udf.mod	afs.mod

# For fshelp.mod.
fshelp_mod_SOURCES = fs/fshelp.c
fshelp_mod_CFLAGS = $(COMMON_CFLAGS)
fshelp_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For fat.mod.
fat_mod_SOURCES = fs/fat.c
fat_mod_CFLAGS = $(COMMON_CFLAGS)
fat_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For ufs.mod.
ufs_mod_SOURCES = fs/ufs.c
ufs_mod_CFLAGS = $(COMMON_CFLAGS)
ufs_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For ext2.mod.
ext2_mod_SOURCES = fs/ext2.c
ext2_mod_CFLAGS = $(COMMON_CFLAGS)
ext2_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For ntfs.mod.
ntfs_mod_SOURCES = fs/ntfs.c
ntfs_mod_CFLAGS = $(COMMON_CFLAGS)
ntfs_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For ntfscomp.mod.
ntfscomp_mod_SOURCES = fs/ntfscomp.c
ntfscomp_mod_CFLAGS = $(COMMON_CFLAGS)
ntfscomp_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For minix.mod.
minix_mod_SOURCES = fs/minix.c
minix_mod_CFLAGS = $(COMMON_CFLAGS)
minix_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For hfs.mod.
hfs_mod_SOURCES = fs/hfs.c
hfs_mod_CFLAGS = $(COMMON_CFLAGS)
hfs_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For jfs.mod.
jfs_mod_SOURCES = fs/jfs.c
jfs_mod_CFLAGS = $(COMMON_CFLAGS)
jfs_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For iso9660.mod.
iso9660_mod_SOURCES = fs/iso9660.c
iso9660_mod_CFLAGS = $(COMMON_CFLAGS)
iso9660_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For xfs.mod.
xfs_mod_SOURCES = fs/xfs.c
xfs_mod_CFLAGS = $(COMMON_CFLAGS)
xfs_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For affs.mod.
affs_mod_SOURCES = fs/affs.c
affs_mod_CFLAGS = $(COMMON_CFLAGS)
affs_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For sfs.mod.
sfs_mod_SOURCES = fs/sfs.c
sfs_mod_CFLAGS = $(COMMON_CFLAGS)
sfs_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For hfsplus.mod.
hfsplus_mod_SOURCES = fs/hfsplus.c
hfsplus_mod_CFLAGS = $(COMMON_CFLAGS)
hfsplus_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For reiserfs.mod.
reiserfs_mod_SOURCES = fs/reiserfs.c
reiserfs_mod_CFLAGS = $(COMMON_CFLAGS)
reiserfs_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For cpio.mod.
cpio_mod_SOURCES = fs/cpio.c
cpio_mod_CFLAGS = $(COMMON_CFLAGS)
cpio_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For tar.mod.
tar_mod_SOURCES = fs/tar.c
tar_mod_CFLAGS = $(COMMON_CFLAGS)
tar_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For udf.mod.
udf_mod_SOURCES = fs/udf.c
udf_mod_CFLAGS = $(COMMON_CFLAGS)
udf_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For afs.mod.
afs_mod_SOURCES = fs/afs.c
afs_mod_CFLAGS = $(COMMON_CFLAGS)
afs_mod_LDFLAGS = $(COMMON_LDFLAGS)

# Partition maps.
pkglib_MODULES += amiga.mod apple.mod pc.mod sun.mod acorn.mod gpt.mod

# For amiga.mod
amiga_mod_SOURCES = partmap/amiga.c
amiga_mod_CFLAGS = $(COMMON_CFLAGS)
amiga_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For apple.mod
apple_mod_SOURCES = partmap/apple.c
apple_mod_CFLAGS = $(COMMON_CFLAGS)
apple_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For pc.mod
pc_mod_SOURCES = partmap/pc.c
pc_mod_CFLAGS = $(COMMON_CFLAGS)
pc_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For sun.mod
sun_mod_SOURCES = partmap/sun.c
sun_mod_CFLAGS = $(COMMON_CFLAGS)
sun_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For acorn.mod
acorn_mod_SOURCES = partmap/acorn.c
acorn_mod_CFLAGS = $(COMMON_CFLAGS)
acorn_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For gpt.mod
gpt_mod_SOURCES = partmap/gpt.c
gpt_mod_CFLAGS = $(COMMON_CFLAGS)
gpt_mod_LDFLAGS = $(COMMON_LDFLAGS)

# Special disk structures and generic drivers

pkglib_MODULES += raid.mod raid5rec.mod raid6rec.mod mdraid.mod dm_nv.mod \
	lvm.mod scsi.mod

# For raid.mod
raid_mod_SOURCES = disk/raid.c
raid_mod_CFLAGS = $(COMMON_CFLAGS)
raid_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For raid5rec.mod
raid5rec_mod_SOURCES = disk/raid5_recover.c
raid5rec_mod_CFLAGS = $(COMMON_CFLAGS)
raid5rec_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For raid6rec.mod
raid6rec_mod_SOURCES = disk/raid6_recover.c
raid6rec_mod_CFLAGS = $(COMMON_CFLAGS)
raid6rec_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For mdraid.mod
mdraid_mod_SOURCES = disk/mdraid_linux.c
mdraid_mod_CFLAGS = $(COMMON_CFLAGS)
mdraid_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For dm_nv.mod
dm_nv_mod_SOURCES = disk/dmraid_nvidia.c
dm_nv_mod_CFLAGS = $(COMMON_CFLAGS)
dm_nv_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For lvm.mod
lvm_mod_SOURCES = disk/lvm.c
lvm_mod_CFLAGS = $(COMMON_CFLAGS)
lvm_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For scsi.mod
scsi_mod_SOURCES = disk/scsi.c
scsi_mod_CFLAGS = $(COMMON_CFLAGS)
scsi_mod_LDFLAGS = $(COMMON_LDFLAGS)

# Commands.
pkglib_MODULES += minicmd.mod extcmd.mod hello.mod handler.mod	\
	ls.mod cmp.mod cat.mod help.mod search.mod loopback.mod	\
	fs_file.mod fs_uuid.mod configfile.mod echo.mod		\
	terminfo.mod test.mod blocklist.mod hexdump.mod		\
	read.mod sleep.mod loadenv.mod crc.mod parttool.mod	\
	pcpart.mod memrw.mod normal.mod sh.mod lua.mod	\
	gptsync.mod true.mod probe.mod

# For gptsync.mod.
gptsync_mod_SOURCES = commands/gptsync.c
gptsync_mod_CFLAGS = $(COMMON_CFLAGS)
gptsync_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For minicmd.mod.
minicmd_mod_SOURCES = commands/minicmd.c
minicmd_mod_CFLAGS = $(COMMON_CFLAGS)
minicmd_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For extcmd.mod.
extcmd_mod_SOURCES = commands/extcmd.c lib/arg.c
extcmd_mod_CFLAGS = $(COMMON_CFLAGS)
extcmd_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For hello.mod.
hello_mod_SOURCES = hello/hello.c
hello_mod_CFLAGS = $(COMMON_CFLAGS)
hello_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For parttool.mod.
parttool_mod_SOURCES = commands/parttool.c
parttool_mod_CFLAGS = $(COMMON_CFLAGS)
parttool_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For pcpart.mod.
pcpart_mod_SOURCES = parttool/pcpart.c
pcpart_mod_CFLAGS = $(COMMON_CFLAGS)
pcpart_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For handler.mod.
handler_mod_SOURCES = commands/handler.c
handler_mod_CFLAGS = $(COMMON_CFLAGS)
handler_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For ls.mod.
ls_mod_SOURCES = commands/ls.c
ls_mod_CFLAGS = $(COMMON_CFLAGS)
ls_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For cmp.mod.
cmp_mod_SOURCES = commands/cmp.c
cmp_mod_CFLAGS = $(COMMON_CFLAGS)
cmp_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For cat.mod.
cat_mod_SOURCES = commands/cat.c
cat_mod_CFLAGS = $(COMMON_CFLAGS)
cat_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For echo.mod
echo_mod_SOURCES = commands/echo.c
echo_mod_CFLAGS = $(COMMON_CFLAGS)
echo_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For help.mod.
help_mod_SOURCES = commands/help.c
help_mod_CFLAGS = $(COMMON_CFLAGS)
help_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For search.mod.
search_mod_SOURCES = commands/search.c
search_mod_CFLAGS = $(COMMON_CFLAGS)
search_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For test.mod.
test_mod_SOURCES = commands/test.c
test_mod_CFLAGS = $(COMMON_CFLAGS)
test_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For loopback.mod
loopback_mod_SOURCES = disk/loopback.c
loopback_mod_CFLAGS = $(COMMON_CFLAGS)
loopback_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For fs_file.mod
fs_file_mod_SOURCES = disk/fs_file.c
fs_file_mod_CFLAGS = $(COMMON_CFLAGS)
fs_file_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For fs_uuid.mod
fs_uuid_mod_SOURCES = disk/fs_uuid.c
fs_uuid_mod_CFLAGS = $(COMMON_CFLAGS)
fs_uuid_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For configfile.mod
configfile_mod_SOURCES = commands/configfile.c
configfile_mod_CFLAGS = $(COMMON_CFLAGS)
configfile_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For terminfo.mod.
terminfo_mod_SOURCES = term/terminfo.c term/tparm.c
terminfo_mod_CFLAGS = $(COMMON_CFLAGS)
terminfo_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For blocklist.mod.
blocklist_mod_SOURCES = commands/blocklist.c
blocklist_mod_CFLAGS = $(COMMON_CFLAGS)
blocklist_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For hexdump.mod.
hexdump_mod_SOURCES = commands/hexdump.c lib/hexdump.c
hexdump_mod_CFLAGS = $(COMMON_CFLAGS)
hexdump_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For read.mod.
read_mod_SOURCES = commands/read.c
read_mod_CFLAGS = $(COMMON_CFLAGS)
read_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For sleep.mod.
sleep_mod_SOURCES = commands/sleep.c
sleep_mod_CFLAGS = $(COMMON_CFLAGS)
sleep_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For loadenv.mod.
loadenv_mod_SOURCES = commands/loadenv.c lib/envblk.c
loadenv_mod_CFLAGS = $(COMMON_CFLAGS)
loadenv_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For crc.mod.
crc_mod_SOURCES = commands/crc.c lib/crc.c
crc_mod_CFLAGS = $(COMMON_CFLAGS)
crc_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For memrw.mod.
memrw_mod_SOURCES = commands/memrw.c
memrw_mod_CFLAGS = $(COMMON_CFLAGS)
memrw_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For true.mod
true_mod_SOURCES = commands/true.c
true_mod_CFLAGS = $(COMMON_CFLAGS)
true_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For probe.mod.
probe_mod_SOURCES = commands/probe.c
probe_mod_CFLAGS = $(COMMON_CFLAGS)
probe_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For normal.mod.
normal_mod_SOURCES = normal/main.c normal/cmdline.c normal/dyncmd.c \
	normal/autofs.c normal/handler.c \
	normal/color.c normal/completion.c normal/datetime.c normal/menu.c \
	normal/menu_entry.c normal/menu_text.c normal/menu_viewer.c \
	normal/misc.c
normal_mod_CFLAGS = $(COMMON_CFLAGS)
normal_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For sh.mod.
sh_mod_SOURCES = script/sh/main.c script/sh/script.c script/sh/execute.c \
	script/sh/function.c script/sh/lexer.c grub_script.tab.c
sh_mod_CFLAGS = $(COMMON_CFLAGS)
sh_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For lua.mod.
lua_mod_SOURCES = script/lua/lapi.c script/lua/lcode.c script/lua/ldebug.c \
	script/lua/ldo.c script/lua/ldump.c script/lua/lfunc.c \
	script/lua/lgc.c script/lua/llex.c script/lua/lmem.c \
	script/lua/lobject.c script/lua/lopcodes.c script/lua/lparser.c \
	script/lua/lstate.c script/lua/lstring.c script/lua/ltable.c \
	script/lua/ltm.c script/lua/lundump.c script/lua/lvm.c \
	script/lua/lzio.c script/lua/lauxlib.c script/lua/lbaselib.c \
	script/lua/linit.c script/lua/ltablib.c script/lua/lstrlib.c \
	script/lua/grub_main.c script/lua/grub_lib.c
lua_mod_CFLAGS = $(COMMON_CFLAGS)
lua_mod_LDFLAGS = $(COMMON_LDFLAGS)

# Extra libraries for lua
# script/lua/lmathlib.c script/lua/loslib.c script/lua/liolib.c
# script/lua/ldblib.c script/lua/loadlib.c

# Common Video Subsystem specific modules.
pkglib_MODULES += video.mod videotest.mod bitmap.mod tga.mod jpeg.mod	\
	png.mod	font.mod gfxterm.mod

# For video.mod.
video_mod_SOURCES = video/video.c
video_mod_CFLAGS = $(COMMON_CFLAGS)
video_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For videotest.mod.
videotest_mod_SOURCES = commands/videotest.c
videotest_mod_CFLAGS = $(COMMON_CFLAGS)
videotest_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For bitmap.mod
bitmap_mod_SOURCES = video/bitmap.c
bitmap_mod_CFLAGS = $(COMMON_CFLAGS)
bitmap_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For tga.mod
tga_mod_SOURCES = video/readers/tga.c
tga_mod_CFLAGS = $(COMMON_CFLAGS)
tga_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For jpeg.mod.
jpeg_mod_SOURCES = video/readers/jpeg.c
jpeg_mod_CFLAGS = $(COMMON_CFLAGS)
jpeg_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For png.mod.
png_mod_SOURCES = video/readers/png.c
png_mod_CFLAGS = $(COMMON_CFLAGS)
png_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For font.mod.
font_mod_SOURCES = font/font_cmd.c font/font.c
font_mod_CFLAGS = $(COMMON_CFLAGS)
font_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For gfxterm.mod.
gfxterm_mod_SOURCES = term/gfxterm.c
gfxterm_mod_CFLAGS = $(COMMON_CFLAGS)
gfxterm_mod_LDFLAGS = $(COMMON_LDFLAGS)

# Misc.
pkglib_MODULES += gzio.mod bufio.mod elf.mod

# For elf.mod.
elf_mod_SOURCES = kern/elf.c
elf_mod_CFLAGS = $(COMMON_CFLAGS)
elf_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For gzio.mod.
gzio_mod_SOURCES = io/gzio.c
gzio_mod_CFLAGS = $(COMMON_CFLAGS)
gzio_mod_LDFLAGS = $(COMMON_LDFLAGS)

# For bufio.mod.
bufio_mod_SOURCES = io/bufio.c
bufio_mod_CFLAGS = $(COMMON_CFLAGS)
bufio_mod_LDFLAGS = $(COMMON_LDFLAGS)

# Misc.
pkglib_MODULES += xnu_uuid.mod

# For elf.mod.
xnu_uuid_mod_SOURCES = commands/xnu_uuid.c
xnu_uuid_mod_CFLAGS = $(COMMON_CFLAGS)
xnu_uuid_mod_LDFLAGS = $(COMMON_LDFLAGS)

pkglib_MODULES += setjmp.mod
setjmp_mod_SOURCES = lib/$(target_cpu)/setjmp.S
setjmp_mod_ASFLAGS = $(COMMON_ASFLAGS)
setjmp_mod_LDFLAGS = $(COMMON_LDFLAGS)
